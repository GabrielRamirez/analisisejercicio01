
global valor_verdadero cifras es contador x resultado iteracion ea fact anterior;

contador = 0; 

disp('=====================================================');
disp('Ejercicio 1: cos(x)=1-(x^2)/2!+(x^4)/4!-(x^6)/6! ...'); 

format long;
valor_verdadero = cos(0.7*pi);
cifras=input('¿Cuantas cifras significativas? ');

while(cifras < 0)
disp('Las cifras significativas no pueden ser negativas'); 
cifras = input('Cuantas cifras significativas?');
end

es=(0.5*10^(2-cifras));

x = input('Ingrese el valor de "x" ');

%disp('iteracion  Respuesta  Error Aprox.')

format long;
resultado =1;
iteracion =1;
ea = 0;

    fprintf('Iteracion          Resultado           Error Aprox \n');
    fprintf(' %.0f                 %g                 %g \n',iteracion,resultado,ea);
    format long;
    anterior = resultado;
    iteracion=iteracion+1;
    contador=contador+2;
    fact = factorial(contador);    
    if(mod(iteracion,2)==0)
    resultado = resultado-((x^contador)/fact);
    else
       resultado = resultado+((x^contador)/fact); 
    end
    ea = (resultado-anterior)/resultado;
    fprintf(' %.0f            %.10f              %.10f \n',iteracion,resultado,ea);
    while(abs(ea)>es)
    anterior = resultado;
    iteracion=iteracion+1;
    contador=contador+2;
    fact = factorial(contador);    
    if(mod(iteracion,2)==0)
    resultado = resultado-((x^contador)/fact);
    else
       resultado = resultado+((x^contador)/fact); 
    end
    ea = ((resultado-anterior)/resultado)*100;
     fprintf(' %.0f            %.10f              %.10f \n',iteracion,resultado,ea);
    end
    
    disp('El valor real es: ');
    disp(valor_verdadero);
    disp('El nivel de tolerancia es: ');
    format short; 
    disp(es);
    format long;
    disp('El valor calculado es: ');
    disp(resultado);
    disp('El error aproximado es: ');
    format short;
    disp(ea);
    

